# [anti-hour](https://gitlab.com/eidoom/anti-hour)

Init: [`create-svelte`](https://github.com/sveltejs/kit/tree/master/packages/create-svelte).

## Octopus Energy API

* [Octopus Energy API](https://developer.octopus.energy/docs/api/)
* [Unofficial Octopus Energy API guide](https://www.guylipman.com/octopus/api_guide.html)

## Developing

Once you've created a project and installed dependencies with `npm install` (or `pnpm install` or `yarn`), start a development server:

```bash
npm run dev
```

## Building

To create a production version of your app using [`@sveltejs/adapter-static`](https://kit.svelte.dev/docs/adapters#supported-environments-static-sites):

```bash
npm run build
```

You can preview the production build with `npm run preview`.

## Dependencies on Debian 11

[Currently need to use `testing` to get up-to-date `nodejs`.](https://eidoom.gitlab.io/computing-blog/post/server-install/#installing-selectively-from-testing).

Hopefully in the future can use backports if `nodejs` 18 is added to it:

> Add to `/etc/apt/sources.list`
> 
> ```vim
> deb http://deb.debian.org/debian bullseye-backports main
> deb-src http://deb.debian.org/debian bullseye-backports main
> ```
> 
> then
> 
> ```shell
> sudo apt install -t bullseye-backports nodejs npm
> ```
> 
> to get a suitably up-to-date `nodejs` environment to compile the site.

## Deploy on remote

```shell
sudo usermod -aG <user> www-data
cd /var/www
sudo mkdir anti-hour
sudo chown www-data:www-data anti-hour
sudo chmod g+w anti-hour
```

On local,

```shell
cd ~/git/anti-hour
./deploy.bash <remote>
```

Ignore failures for `/var/www/anti-hour/.`.

## Nginx reverse proxy for statically built site

Sample `/etc/nginx/sites-available/anti-hour.conf` using [DNS-01 TLS](https://gitlab.com/eidoom/cloud-infrastructure/-/tree/main/type/__tls_wildcard_duckdns/gencode-remote):

```nginx
server {
    listen 80;
    listen [::]:80;

    server_name anti-hour.HOSTNAME;

    location / {
        return 301 https://$host$request_uri;
    }
}

server {
    listen 443 ssl http2;
    listen [::]:443 ssl http2;

    server_name anti-hour.HOSTNAME;

    ssl_certificate /etc/letsencrypt/live/star.HOSTNAME/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/star.HOSTNAME/privkey.pem;

    root /PATH/TO/anti-hour/build/;

    index index.html;

    location / {
        try_files $uri $uri/ =404;
    }
}
```
