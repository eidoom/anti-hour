#!/usr/bin/env bash

echo Syncing to remote $1
echo

npm run build

rsync --archive --human-readable --compress --partial --info=progress2 --delete build/ $1:/var/www/anti-hour
