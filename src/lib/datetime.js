export const formatter = new Intl.DateTimeFormat([], {
  weekday: "short",
  hour: "2-digit",
  minute: "2-digit",
});
