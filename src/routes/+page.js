export const prerender = true;

export async function load({ fetch, params }) {
  const base_url = "https://api.octopus.energy",
    product_code = "AGILE-23-12-06",
    region = "P", // northern scotland
    register = "E-1R", // single electricity register
    tariff_code = `${register}-${product_code}-${region}`,
    tariff_url = `${base_url}/v1/products/${product_code}/electricity-tariffs/${tariff_code}`,
    time = new Date(),
    response = await fetch(
      `${tariff_url}/standard-unit-rates/?period_from=${time.toISOString()}`,
    ),
    json = await response.json();

  let rates = json.results;

  const largest = rates.reduce(
      (acc, cur) => Math.max(acc, cur.value_inc_vat),
      0,
    ),
    smallest = rates.reduce((acc, cur) => Math.min(acc, cur.value_inc_vat), 0),
    range = largest - smallest,
    scale = 100;

  rates = rates
    .map((e) => ({
      value: e.value_inc_vat,
      valid_from: new Date(e.valid_from),
      size: Math.max(1, Math.abs((scale * e.value_inc_vat) / range)),
      pos: e.value_inc_vat >= 0,
    }))
    .reverse();

  return { rates, time, offset: Math.abs((scale * smallest) / range) };
}
